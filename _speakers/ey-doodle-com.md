---
name: ey@doodle.com
talks:
- GREAT ENGINEERING IN THE WORST WORK ENVIRONMENT
---

Python developer and engineering lead in Doodle.
Served in the Israeli Intelligence Unit, worked in IdmLogic (Acquired by CA technologies) and co-founded Meekan (Acquired by Doodle).
Main passion is for connecting systems together, and making things talk to each other in a nice fashion.