---
name: Binod Gyawali
talks:
- Building a Read Aloud eBook in Python
---

Binod Gyawali is a research engineer in ETS NLP and Speech group. He holds a Master’s degree in Computer Science with concentration in NLP and has been using Python for various NLP tasks.
