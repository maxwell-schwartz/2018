---
name: "Fl\xE1vio Juvenal"
talks:
- 1 + 1 = 1 or Record Deduplication with Python
---

I'm a software engineer from Brazil and partner at Vinta Software (www.vinta.com.br). At Vinta, we love beautiful high-quality products, from UX to code, and we will defend them against unreasonable deadlines and features. We use mostly React and Django.