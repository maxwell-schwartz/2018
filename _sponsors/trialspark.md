---
name: Trialspark
tier: social event
site_url: https://www.trialspark.com/careers
logo: trialspark.png
---
Trialspark is a digital health company that accelerates the discovery of new treatments for patients
by reimagining the clinical trial process.

Today, clinical trials are incredibly inefficient. A single clinical trial can cost more than 100
million dollars and less than 10% of clinical trials are completed on time. Our mission is to bring
treatments to patients faster by addressing these inefficiencies.
