---
abstract: "Jane Austen said, \u201CIt is a truth universally acknowledged, that a
  single module in possession of a good function, must be in want of a test.\u201D
  In this talk, we\u2019ll discover how writing tips from your high school English
  class can make your code cleaner and clearer."
level: All
speakers:
- Lacey Williams Henschel
title: 'What if Jane Austen had been an engineer? '
type: talk
---

As a developer with two English degrees, over the years I’ve identified some concrete ways this education makes me a better developer. This talk will discuss how we can take lessons from literature to write more readable code, make better tests, and create more usable websites. At the end of the day, after all, our job is to write. Surely the techniques of great writers have something to teach us. 

In this humanities-based talk, you'll learn about Freytag's pyramid and what it has to do with writing clean code, why writing those dastardly outlines before your term papers helped prepare you for your job as an engineer, and see for yourself how _The Zen of Python_ is basically a modern retelling of Strunk and White's classic _Elements of Style_. You will leave inspired to go read your favorite book to get some tips for your next project.