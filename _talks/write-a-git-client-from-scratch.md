---
abstract: What better way to learn the fundamentals of `git` than to reimplement them
  yourself? In this talk, we will (hurridly) cover the theory behind `git` and build
  a minimal `git` client in Python from scratch without anything but the standard
  library and our wits! 100% live coded. Accept no substitutes.
level: Intermediate
speakers:
- James Powell
title: Write a Git Client from Scratch
type: talk
---

`git` is a powerful tool with a confusing interface. Many struggle with commands beyond `git add`, `git commit`, `git pull`, and `git push`, because the underlying model for `git` is poorly communicated. But once a user has a thorough understanding of `git` internals, the reasoning behind its (admittedly rough) interface becomes much clearer, and the path to mastery becomes clear.

Rather than present the internals of `git` through dry explanation, this talk explains the internals of `git` through live coding and live interaction with a `git` repo. Our goal: to write a minimal `git` client from scratch without the help of any "heavy" libraries like `libgit2`. Instead, we'll cover the contents of `./.git`, how to interact with them, the "theory" behind `git` (e.g., what does it mean that `git` is an immutable, content-addressible store?), and slowly develop a tool to do basic `git` operations.